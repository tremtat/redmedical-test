// hack to make JSON imports work with TS
// I think the current version of TS supports JSON imports
// but it is incompatible with this version of Angular...

import * as jsonData from 'app/api/weather/weatherdata.json';
import { WeatherDataItem } from './definitions.js';

const untypedWeatherData = jsonData as any;
export const typedWeatherData = untypedWeatherData as WeatherDataItem[];