export interface SO_Owner {
    accept_rate: number
    display_name: string
    link: string
    profile_image: string
    user_id: number
    user_type: string
}

export interface SO_Question {
    answer_count: number
    creation_date: number
    is_answered: boolean
    last_activity_date: number
    last_edit_date: number
    link: string
    owner: SO_Owner
    question_id: number
    score: number
    tags: string[]
    title: string
    view_count: number
}

export interface SO_ApiResp {
    has_more: boolean;
    items: SO_Question[];
    quota_max: number;
    quota_remaining: number;
}