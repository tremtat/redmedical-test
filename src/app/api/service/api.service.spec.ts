import { TestBed, inject } from '@angular/core/testing';

import { ApiService } from './api.service';
import {HttpModule} from "@angular/http";
import {APP_BASE_HREF} from "@angular/common";

describe('ApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        ApiService
      ]
    });
  });

  it('should ...', inject([ApiService], (service: ApiService) => {
    expect(service).toBeTruthy();
  }));
});
