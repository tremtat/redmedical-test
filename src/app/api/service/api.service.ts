import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import { SO_Question, SO_ApiResp } from "../stack-overflow/definitions";

@Injectable()
export class ApiService {

    private static readonly apiUrl =
        "https://api.stackexchange.com/2.2/search?pagesize=20&order=desc&sort=activity&site=stackoverflow&intitle=";

    constructor(private http: Http) { }

    search(keyword: string): Promise<SO_Question[]> {
        return new Promise((resolve, reject) => {
            
            this.http.get(ApiService.apiUrl + keyword)
            .subscribe((res: Response) => {
                let data = res.json() as SO_ApiResp;
                console.log("API USAGE: " + data.quota_remaining + " of " + data.quota_max + " requests available" );
                resolve(data.items);
            },
            (err: Response) => {
                reject(err.json())
            })
        });
    }


}
