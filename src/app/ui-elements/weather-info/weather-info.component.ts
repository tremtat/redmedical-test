import { Component, Input } from '@angular/core';
import { WeatherDataItem } from 'app/api/weather/definitions';

@Component({
  selector: 'app-weather-info',
  templateUrl: './weather-info.component.html',
  styleUrls: ['./weather-info.component.scss']
})
export class WeatherInfoComponent {
  @Input() item: WeatherDataItem;

}
