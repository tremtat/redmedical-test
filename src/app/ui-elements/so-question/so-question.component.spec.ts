import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoQuestionComponent } from './so-question.component';

describe('SoQuestionComponent', () => {
  let component: SoQuestionComponent;
  let fixture: ComponentFixture<SoQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
