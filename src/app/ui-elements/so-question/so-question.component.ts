import { Component, Input } from '@angular/core';
import { SO_Question } from 'app/api/stack-overflow/definitions';

@Component({
  selector: 'app-so-question',
  templateUrl: './so-question.component.html',
  styleUrls: ['./so-question.component.scss']
})
export class SoQuestionComponent {
  @Input() item: SO_Question;
}
