import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/api/service/api.service';
import { SO_Question } from 'app/api/stack-overflow/definitions';
import { WeatherDataItem } from 'app/api/weather/definitions';
import { typedWeatherData as weatherData } from 'app/api/weather/weatherdata';

interface ISoQuestions {
  [key:string]: SO_Question[];
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [ApiService]
})
export class DashboardComponent implements OnInit {
  soQuestions: ISoQuestions = {};
  weatherData: WeatherDataItem[] = [];

  constructor(private _apiService: ApiService) { }

  async ngOnInit() {
    await this.getSoData();
    await this.getWeatherData();
  }

  async getSoData() {
    const [typescript, angular2, weather] = await Promise.all([
      this._apiService.search('typescript'),
      this._apiService.search('angular2'),
      this._apiService.search('weather')
    ]);
    
    this.soQuestions = { typescript, angular2, weather };
  }

  async getWeatherData() {
    const weatherDataExtract = [];
    for (let i = 0; i < 5; i++) {
      weatherDataExtract.push(weatherData[Math.floor(Math.random() * weatherData.length)])
    }
    this.weatherData = weatherDataExtract
  }

}
