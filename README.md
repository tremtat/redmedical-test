# redmedical-unicorn

I want to list here some points about how I approached this task:

there is a couple of things with the code that should be fixed but I think it is out of the scope of this task. Normally I would report them and create a new task in whichever management tool is used.

- dependencies: should be updated. In my experience update a Angular CLI is a big pain because of the dependencies mismatches so I would go for the radical approache. I would create a whole new project with the latest version of the CLI and copy over the app code in `/src`
- layout has a bug. Below 993px wide, scrolling in the dashboard is not possible because the the `.main-content` CSS class has the `position` property set to `fixed`; Above 993px, the header looks funny because its position is too much on the left; The `.topbar` class is missing a `left: $sidebar-width` property.
- I don't think it's a good idea to put the SCSS files in the `/src/assets` folder, since this folder is entirely copied over to the production build.


When I make a plain HTTP call, I prefer to get a `promise` rather than an Observable because there is just one response, no _stream_ of data. An observable would be misleading.


I like to have all the logic concentrated in a few components, and keep the other components _dumb_, stateless and logic-less. I believe it makes it much easier to debug. In many cases, even in a single page app, I find the component corresponding to a main route (what we might call a view or a page) to be a good choice. In this case, I chose to make the API calls and handle the app logic in the DashboardComponent. The components in the `/src/app/ui-elements` are purely presentational. 

I would recommend to use the opportunity Angular offers to scope the styles instead of having global-scoped styles.

There are many things that could be improved if I spent more time on the task, for instance:
- the UI can be improved a great deal,
- the search service could be refactored so it is possible to get only a given number of items (instead of 20 like now)
- I forgot to add the units to the weather data.
- ...

But IMO the point of this test is not to spend days of work to make something perfect, but for you to see how I code. Hope you'll enjoy ;-)